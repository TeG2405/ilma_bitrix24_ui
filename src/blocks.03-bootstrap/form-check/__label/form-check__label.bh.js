module.exports = function (bh) {
    bh.match('form-check__label', function (ctx, json) {
        ctx
            .tag('label')
            .attr('for', ctx.tParam('ID'))
    })
}