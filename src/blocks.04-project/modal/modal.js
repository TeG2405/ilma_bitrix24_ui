import {Modal} from 'bootstrap';

var stack = [];
$(document).on('show.bs.modal', '.modal',  function (e) {
    e.target != stack[stack.length - 1] && stack.push(e.target);
    if(stack.length > 1){
        $(stack[stack.length - 2]).modal('hide');
        $('html').addClass('modal-open');
    }
});
$(document).on('hide.bs.modal', '.modal',  function (e) {
    if(e.target == stack[stack.length - 1] && stack.pop()){
        $(stack[stack.length - 1]).modal('show');
    }
    if(!stack.length){
        $('html').removeClass('modal-open');
    }

});