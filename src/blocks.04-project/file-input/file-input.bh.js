module.exports = function (bh) {
    bh.match('file-input', function (ctx, json) {
        ctx.tParam('ID', ctx.generateId());
        ctx.tag('label').attr('for', ctx.tParam('ID')).content([
            ctx.content(),
            {tag: 'input', attrs: {type: 'file', id: ctx.tParam('ID')}}
        ], ctx.isSimple())
    })
}