module.exports = function (bh) {
    bh.match('filter-list', function (ctx, json) {
        ctx.tag('ul').content(
            ctx.content().map((item)=>{
                if(ctx.isSimple(item)) return {elem: 'li', content: item};
                return item
            }),
            true
        );
    })
}