//<script[\s\S]*?>[\s\S]*?<\/script>
module.exports = function (bh) {
    bh.match('environment', function (ctx, json) {
        let content = bh.apply(ctx.content());
        return `
        <!DOCTYPE html>
<html class="bx-core bx-no-touch bx-no-retina bx-chrome">
<head>
    <meta name="viewport" content="width=1135">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-itunes-app" content="app-id=561683423">
    <link rel="apple-touch-icon-precomposed" href="https://cpiv.bitrix24.ru/images/iphone/57x57.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://cpiv.bitrix24.ru/images/iphone/72x72.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://cpiv.bitrix24.ru/images/iphone/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://cpiv.bitrix24.ru/images/iphone/144x144.png">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/core/css/core.min.css?14290239272854" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/core/css/core_popup.min.css?150876739513169" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/core/css/core_date.min.css?14655473279657" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/core/css/core_tooltip.min.css?14909536447772" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/sidepanel/css/sidepanel.min.css?15087673953169" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/intranet/sidepanel/css/slider.min.css?151194279014512" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/intranet/theme_picker/theme_picker.min.css?15071261909563" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/calendar/core_planner_handler.css?14165556384197" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/im/css/common.min.css?151427860345202" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/im/css/window.min.css?151792145317219" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/im/css/im.min.css?1521195763194096" type="text/css" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/cache/css/s1/bitrix24/default_732aa91777ca4de2bb4c3b0d7c39fe9d/default_732aa91777ca4de2bb4c3b0d7c39fe9d.css?1521477709898"
          type="text/css" rel="stylesheet">
    <link type="text/css" rel="stylesheet"
          href="https://cpiv.bitrix24.ru/bitrix/components/bitrix/main.interface.buttons/templates/.default/style.min.css?151801858414932">
    <link type="text/css" rel="stylesheet"
          href="https://cpiv.bitrix24.ru/bitrix/components/bitrix/app.layout/templates/.default/style.min.css?1488266694824">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/intranet/intranet-common.min.css?150850189661344" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/colorpicker/css/colorpicker.min.css?14934546003209" type="text/css"
          data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/tasks/css/media.min.css?14867361429205" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/core/css/core_viewer.min.css?151056902357920" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/tasks/css/tasks.min.css?1499785558103913" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/components/bitrix/tasks.iframe.popup/templates/.default/style.min.css?1469798598820"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/intranet/core_planner.min.css?1507126190767" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/timeman/css/core_timeman.min.css?149520703547528" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/core/css/core_finder.min.css?151056902323691" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/components/bitrix/system.auth.form/.default/style.min.css?1517906542560"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/map.min.css?15066903313149"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/groups.min.css?15066903316255"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/style.min.css?151790654222283"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/main/phonenumber/css/phonenumber.min.css?150876739535914" type="text/css"
          data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/rest/css/applayout.min.css?1488266694856" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/im/css/phone_call_view.min.css?151427860344650" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/components/bitrix/crm.card.show/templates/.default/style.min.css?149855178912014"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/disk/css/disk.min.css?151438857368365" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/js/disk/css/file_dialog.min.css?14952070498326" type="text/css" data-template-style="true"
          rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/components/bitrix/im.messenger/.default/style.min.css?150782189012421"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/components/bitrix/bitrix24.notify.panel/templates/.default/style.min.css?15168032442348"
          type="text/css" data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/template_styles.min.css?151790656289076" type="text/css"
          data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/interface.min.css?1517906562102981" type="text/css"
          data-template-style="true" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/themes/.default/clock.min.css?14290239275748" type="text/css" data-template-style="true"
          rel="stylesheet">


    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/themes/default/main.css?150712619019982" type="text/css"
          data-template-style="true" data-theme-id="default" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/themes/default/menu.css?15066903316858" type="text/css"
          data-template-style="true" data-theme-id="default" rel="stylesheet">
    <link href="https://cpiv.bitrix24.ru/bitrix/templates/bitrix24/themes/default/messenger.css?15066903311444" type="text/css"
          data-template-style="true" data-theme-id="default" rel="stylesheet">


    <title>${json.title}</title>
</head>
<body class="template-bitrix24 bitrix24-default-theme">
<div id="bxdynamic_title_start" style="display:none"></div>
<div id="bxdynamic_title_end" style="display:none"></div>
<div class="bx-desktop bx-im-fullscreen-popup bx-im-fullscreen-closed bx-im-fullscreen-popup-transparent"
     id="im-workarea-popup">
    <table class="bx-im-fullscreen-popup-table">
        <tbody>
        <tr>
            <td class="bx-im-fullscreen-popup-td bx-im-fullscreen-popup-td1">
                <div class="bx-im-fullscreen-popup-logo">
                    <span>															ИНТЕРВОЛГА 24													</span>
                </div>
                <div class="bx-im-fullscreen-popup-back"><a href="https://cpiv.bitrix24.ru/" onclick="bxFullscreenClose(); return false;"
                                                            class="bx-im-fullscreen-popup-back-link">Закрыть</a></div>
            </td>
        </tr>
        <tr>
            <td class="bx-im-fullscreen-popup-td bx-im-fullscreen-popup-td2">
                <div class="bx-desktop-placeholder bx-desktop" id="im-workarea-content">
                    <div class="bx-desktop-appearance" style="min-height: 384px;">
                        <div class="bx-desktop-appearance-menu">
                            <div class="bx-desktop-appearance-avatar"><a href="https://cpiv.bitrix24.ru/company/personal/user/25/"
                                                                         title="Артём Звездилин" target="_blank"
                                                                         class="bx-desktop-avatar"><img
                                    src="https://bitrix2.cdnvideo.ru/b117697/resize_cache/51795/7acf4cadf975128573a8b1c2766af5d8/main/c32/c321e4834db6dcda1b6d2062f1545b82/x_c08d7f54.jpg?h=cpiv.bitrix24.ru"
                                    class="bx-desktop-avatar-img bx-desktop-avatar-img-default"></a></div>
                            <div class="bx-desktop-appearance-tab">
                                <div data-id="im" id="bx-desktop-tab-im" title="Сообщения "
                                     class="bx-desktop-tab bx-desktop-tab-im bx-desktop-tab-active"><span
                                        class="bx-desktop-tab-counter"></span>
                                    <div class="bx-desktop-tab-icon bx-desktop-tab-icon-im"></div>
                                </div>
                                <div data-id="notify" id="bx-desktop-tab-notify" title="Уведомления"
                                     class="bx-desktop-tab bx-desktop-tab-notify"><span
                                        class="bx-desktop-tab-counter"></span>
                                    <div class="bx-desktop-tab-icon bx-desktop-tab-icon-notify"></div>
                                </div>
                                <div data-id="im-phone" id="bx-desktop-tab-im-phone" title="Телефонный звонок"
                                     class="bx-desktop-tab bx-desktop-tab-im-phone"><span
                                        class="bx-desktop-tab-counter"></span>
                                    <div class="bx-desktop-tab-icon bx-desktop-tab-icon-im-phone"></div>
                                </div>
                                <div data-id="config" id="bx-desktop-tab-config" title="Настройки"
                                     class="bx-desktop-tab bx-desktop-tab-config"><span
                                        class="bx-desktop-tab-counter"></span>
                                    <div class="bx-desktop-tab-icon bx-desktop-tab-icon-config"></div>
                                </div>
                                <div data-id="sep1521552446567" id="bx-desktop-sep-sep1521552446567"
                                     class="bx-desktop-separator"></div>
                                <div data-id="im-lf" id="bx-desktop-tab-im-lf" title="Живая лента "
                                     class="bx-desktop-tab bx-desktop-tab-im-lf"><span
                                        class="bx-desktop-tab-counter"></span>
                                    <div class="bx-desktop-tab-icon bx-desktop-tab-icon-im-lf"></div>
                                </div>
                            </div>
                        </div>
                        <div class="bx-desktop-appearance-content">
                            <div data-id="im" id="bx-desktop-tab-content-im"
                                 class="bx-desktop-tab-content bx-desktop-tab-content-im bx-desktop-tab-content-active"></div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="bx-im-fullscreen-popup-td bx-im-fullscreen-popup-td3">
				<span class="bx-im-fullscreen-apps">
					<span class="bx-im-fullscreen-apps-title">Установите приложение:</span>
					<span class="bx-im-fullscreen-apps-buttons" id="im-workarea-apps">
						<span class="bx-im-fullscreen-apps-buttons-group">
							<a href="https://cpiv.bitrix24.ruhttp://dl.bitrix24.com/b24/bitrix24_desktop.exe"
                               class="bx-im-fullscreen-app-icon bx-im-fullscreen-app-windows" target="_blank"></a>
							<span class="bx-im-fullscreen-apps-buttons-delimiter"></span>
							<a href="https://cpiv.bitrix24.ruhttp://dl.bitrix24.com/b24/bitrix24_desktop.dmg"
                               class="bx-im-fullscreen-app-icon bx-im-fullscreen-app-osx" target="_blank"></a>
							<span class="bx-im-fullscreen-apps-buttons-delimiter"></span>
							<a href="https://cpiv.bitrix24.ruhttps://github.com/buglloc/brick/"
                               class="bx-im-fullscreen-app-icon bx-im-fullscreen-app-linux" target="_blank"></a>
						</span>
						<span class="bx-im-fullscreen-apps-buttons-group">
							<a href="https://cpiv.bitrix24.ruhttps://play.google.com/store/apps/details?id=com.bitrix24.android"
                               class="bx-im-fullscreen-app-icon bx-im-fullscreen-app-googleplay" target="_blank"></a>
							<span class="bx-im-fullscreen-apps-buttons-delimiter"></span>
							<a href="https://cpiv.bitrix24.ruhttps://itunes.apple.com/ru/app/bitrix24/id561683423"
                               class="bx-im-fullscreen-app-icon bx-im-fullscreen-app-appstore" target="_blank"></a>
						</span>
					</span>
				</span>
				<span class="bx-im-fullscreen-bg">
					<span class="bx-im-fulsscrenn-bg-title">Фон:</span>
					<span class="bx-im-fulsscrenn-bg-wrap">
						<span id="im-workarea-backgound-selector-title" class="bx-im-fulsscrenn-bg-wrap-title">Прозрачный</span>
						<select id="im-workarea-backgound-selector" class="bx-im-fullscreen-bg-selector">
							<option value="transparent">Прозрачный</option>
							<option value="0">Битрикс24</option>
							<option value="1">Камни</option>
							<option value="2">Автомобиль</option>
							<option value="3">Дорога</option>
							<option value="4">Дюны</option>
							<option value="5">Природа</option>
							<option value="6">Снежные вершины</option>
							<option value="7">Колос</option>
							<option value="8">Горное озеро</option>
							<option value="9">Яхта в море</option>
							<option value="10">Париж</option>
							<option value="11">Пляж</option>
							<option value="12">Небо</option>
						</select>
					</span>
				</span>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<table class="bx-layout-table">
    <tbody>
    <tr>
        <td class="bx-layout-header">
            <div id="header">
                <div id="header-inner">

                    <div class="timeman-container timeman-container-ru" id="timeman-container">
                        <div class="timeman-wrap"><span id="timeman-block" class="timeman-block timeman-start"><span
                                class="bx-time" id="timeman-timer"><span class="time-hours">17</span><span
                                class="time-semicolon">:</span><span class="time-minutes">16</span></span><span
                                class="timeman-right-side" id="timeman-right"><span class="timeman-info"
                                                                                    id="timeman-info"
                                                                                    style="display:none"><span
                                class="timeman-event" id="timeman-event" style="display:none"></span><span
                                class="timeman-tasks" id="timeman-tasks" style="display:none">0</span></span><span
                                class="timeman-task-time" id="timeman-task-time" style="display:none"><i></i><span
                                id="timeman-task-timer"></span></span><span class="timeman-beginning-but"
                                                                            id="timeman-status-block"><i></i><span
                                id="timeman-status">Начать</span></span>
				
				</span><span class="timeman-not-closed-block"><span class="timeman-not-cl-icon"></span><span
                                class="timeman-not-cl-text">Вы <strong>не закрыли</strong><br>предыдущий рабочий день.</span></span><span
                                class="timeman-background" id="timeman-background" style="display: block;"></span>
		</span>
                        </div>
                    </div>                    <!--suppress CheckValidXmlInScriptTagBody -->

                    <div class="header-logo-block">
                        <div class="sitemap-menu" id="sitemap-menu"><span class="sitemap-menu-lines"></span></div>
                        <span class="header-logo-block-util"></span>
                        <a id="logo_24_a" href="https://cpiv.bitrix24.ru/" title="На главную страницу" class="logo">								<span
                                id="logo_24_text">
									<span class="logo-text">ИНТЕРВОЛГА</span><span class="logo-color">24</span>								</span>
								<span class="logo-img-span">
									<img id="logo_24_img" src="https://cpiv.bitrix24.ru" style="display:none;">
								</span>
                        </a>
                    </div>


                    <div class="header-search">
                        <div class="header-search-inner">
                            <form class="header-search-form" method="get" name="search-form" action="/search/index.php"
                                  id="search">
                                <input class="header-search-input" name="q" id="search-textbox-input" type="text"
                                       autocomplete="off" placeholder="искать сотрудника, документ, прочее..."
                                       onclick="BX.addClass(this.parentNode.parentNode.parentNode,'header-search-active')"
                                       onblur="BX.removeClass(this.parentNode.parentNode.parentNode, 'header-search-active')">
                                <span class="header-search-icon"
                                      onclick="document.forms['search-form'].submit();"></span>
                            </form>
                        </div>
                    </div>

                    <div id="bxdynamic_XdqEv1_start" style="display:none"></div>
                    <div id="bxdynamic_XdqEv1_end" style="display:none"></div>


                    <div class="user-block" id="user-block" onclick="showUserMenu()">
                        <span class="user-img user-default-avatar"
                              style="background: url('https://bitrix2.cdnvideo.ru/b117697/resize_cache/51795/7acf4cadf975128573a8b1c2766af5d8/main/c32/c321e4834db6dcda1b6d2062f1545b82/x_c08d7f54.jpg?h=cpiv.bitrix24.ru') no-repeat center; background-size: cover;"></span><span
                            class="user-name" id="user-name">Артём Звездилин</span>
                    </div>
                    <div class="help-block" id="bx-help-block" title="Помощь">
                        <div class="help-icon-border"></div>
                        <div class="help-block-icon"></div>
                        <div class="help-block-counter-wrap" id="bx-help-notify">
                            <div class="help-block-counter">2</div>
                        </div>
                    </div>

                    <div id="bxdynamic_b24_helper_start" style="display:none"></div>


                    <div id="bxdynamic_b24_helper_end" style="display:none"></div>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="bx-layout-cont">
            <table class="bx-layout-inner-table im-bar-mode">
                <tbody>
                <tr class="bx-layout-inner-top-row">
                    <td class="bx-layout-inner-left" id="layout-left-column">
                        <div class="menu-items-block" id="bx-left-menu">
                            <div class="menu-resize-container" id="left-menu-resizer">
                                <div class="menu-resize-item" id="left-menu-resizer-button" style="opacity: 0;">
                                    <span class="menu-resize-btn"></span>
                                </div>
                            </div>
                            <ul class="menu-items" id="left-menu-list">
                                <li class="menu-items-empty-li" id="left-menu-empty-item" style="height: 3px;"></li>
                                <li id="bx_left_menu_menu_live_feed" data-status="show" data-id="menu_live_feed"
                                    data-counter-id="**" data-link="/stream/" data-all-links="" data-type="default"
                                    data-delete-perm="N" data-new-page="N" class="menu-item-block menu-item-live-feed">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_live_feed')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/stream/" title="Живая лента" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Живая лента															</span><span
                                            class="menu-item-index-wrap"><span class="menu-item-index"
                                                                               id="menu-counter-live-feed"></span>
								</span>
                                    </a>
                                </li>
                                <li id="bx_left_menu_menu_tasks" data-status="show" data-id="menu_tasks"
                                    data-counter-id="tasks_total" data-link="/company/personal/user/25/tasks/"
                                    data-all-links="/company/personal/user/25/tasks/" data-type="default"
                                    data-delete-perm="N" data-top-menu-id="tasks_panel_menu" data-new-page="N"
                                    class="menu-item-block menu-item-with-index">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_tasks')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

													<span class="menu-item-plus">
								<a href="https://cpiv.bitrix24.ru/company/personal/user/25/tasks/task/edit/0/" class="menu-item-plus-icon"></a>
							</span>
                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/company/personal/user/25/tasks/"
                                       title="Задачи и Проекты" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Задачи и Проекты															</span><span
                                            class="menu-item-index-wrap"><span class="menu-item-index"
                                                                               id="menu-counter-tasks_total">2</span>
								</span>
                                    </a>
                                </li>
                                <li id="bx_left_menu_menu_im_messenger" data-status="show" data-id="menu_im_messenger"
                                    data-counter-id="im-message" data-link="/online/" data-all-links=""
                                    data-type="default" data-delete-perm="N" data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_im_messenger')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/online/" title="Чат и звонки" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Чат и звонки															</span><span
                                            class="menu-item-index-wrap"><span class="menu-item-index"
                                                                               id="menu-counter-im-message"></span>
								</span>
                                    </a>
                                </li>
                                <li id="bx_left_menu_menu_all_groups" data-status="show" data-id="menu_all_groups"
                                    data-counter-id="" data-link="/workgroups/"
                                    data-all-links="/workgroups/,/workgroups/menu/" data-type="default"
                                    data-delete-perm="N" data-top-menu-id="sonetgroups_panel_menu" data-new-page="N"
                                    class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_all_groups')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

													<span class="menu-item-plus">
								<a href="https://cpiv.bitrix24.ru/company/personal/user/25/groups/create/" class="menu-item-plus-icon"></a>
							</span>
                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/workgroups/" title="Группы" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Группы															</span> </a>
                                    <span class="menu-item-show-link" id="menu-all-groups-link">развернуть</span></li>
                                <li id="bx_left_menu_menu_calendar" data-status="show" data-id="menu_calendar"
                                    data-counter-id="calendar" data-link="/company/personal/user/25/calendar/"
                                    data-all-links="/company/personal/user/25/calendar/,/calendar/" data-type="default"
                                    data-delete-perm="N" data-top-menu-id="top_menu_id_calendar" data-new-page="N"
                                    class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_calendar')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/company/personal/user/25/calendar/"
                                       title="Календарь" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Календарь															</span><span
                                            class="menu-item-index-wrap"><span class="menu-item-index"
                                                                               id="menu-counter-calendar"></span>
								</span>
                                    </a>
                                </li>
                                <li id="bx_left_menu_menu_files" data-status="show" data-id="menu_files"
                                    data-counter-id="" data-link="/company/personal/user/25/disk/path/"
                                    data-all-links="/company/personal/user/25/disk/path/,/company/personal/user/25/disk/volume/,/docs/"
                                    data-type="default" data-delete-perm="N" data-top-menu-id="top_menu_id_docs"
                                    data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_files')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/company/personal/user/25/disk/path/" title="Диск"
                                       onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Диск															</span> </a>
                                </li>
                                <li id="bx_left_menu_menu_external_mail" data-status="show" data-id="menu_external_mail"
                                    data-counter-id="mail_unseen" data-link="/company/personal/mail/" data-all-links=""
                                    data-type="default" data-delete-perm="N" data-new-page="N"
                                    class="menu-item-block menu-item-with-index">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_external_mail')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/company/personal/mail/" title="Почта" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							" target="_blank">
							<span class="menu-item-link-text" data-role="item-text">
								Почта															</span><span
                                            class="menu-item-index-wrap"><span class="menu-item-index"
                                                                               id="menu-counter-mail_unseen">99+</span>
								</span>
                                        <span onclick="window.location.replace('/company/personal/mail/?config'); return false; "
                                              title="Изменить настройки" class="menu-post-warn-icon"
                                              id="menu-counter-warning-mail_unseen"></span>
                                    </a>
                                </li>
                                <li id="bx_left_menu_menu_company" data-status="show" data-id="menu_company"
                                    data-counter-id="" data-link="/company/vis_structure.php" data-all-links="/company/"
                                    data-type="default" data-delete-perm="N" data-top-menu-id="top_menu_id_company"
                                    data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_company')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/company/vis_structure.php" title="Компания"
                                       onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Компания															</span> </a>
                                </li>
                                <li id="bx_left_menu_menu_timeman_sect" data-status="show" data-id="menu_timeman_sect"
                                    data-counter-id="" data-link="/timeman/" data-all-links="/timeman/"
                                    data-type="default" data-delete-perm="N" data-top-menu-id="top_menu_id_timeman"
                                    data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_timeman_sect')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/timeman/" title="Время и отчеты" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Время и отчеты															</span> </a>
                                </li>
                                <li id="bx_left_menu_menu_marketplace_sect" data-status="show"
                                    data-id="menu_marketplace_sect" data-counter-id="" data-link="/marketplace/"
                                    data-all-links="/marketplace/" data-type="default" data-delete-perm="N"
                                    data-top-menu-id="top_menu_id_marketplace" data-new-page="N"
                                    class="menu-item-block menu-item-active">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_marketplace_sect')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/marketplace/" title="Приложения" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Приложения															</span> </a>
                                </li>
                                <li id="bx_left_menu_menu_onec_sect" data-status="show" data-id="menu_onec_sect"
                                    data-counter-id="" data-link="/onec/" data-all-links="/onec/" data-type="default"
                                    data-delete-perm="N" data-top-menu-id="top_menu_id_onec" data-new-page="N"
                                    class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_onec_sect')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/onec/" title="1С + CRM Битрикс24" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								1С + CRM Битрикс24															</span>
                                    </a>
                                </li>
                                <li id="bx_left_menu_menu_sites" data-status="show" data-id="menu_sites"
                                    data-counter-id="" data-link="/sites/" data-all-links="" data-type="default"
                                    data-delete-perm="N" data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_sites')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/sites/" title="Сайты" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Сайты																<span
                                    class="menu-item-link-text-beta">beta</span>
															</span> </a>
                                </li>
                                <li id="bx_left_menu_menu_marketing" data-status="show" data-id="menu_marketing"
                                    data-counter-id="" data-link="/marketing/" data-all-links="" data-type="default"
                                    data-delete-perm="N" data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_marketing')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/marketing/" title="CRM-маркетинг" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								CRM-маркетинг																<span
                                    class="menu-item-link-text-beta">beta</span>
															</span> </a>
                                </li>
                                <li id="bx_left_menu_3680968806" data-status="show" data-id="3680968806"
                                    data-counter-id="" data-link="/marketplace/app/38/"
                                    data-all-links="/marketplace/app/intervolga.ac700/" data-type="admin"
                                    data-delete-perm="A" data-new-page="N" class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, '3680968806')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                    <a class="menu-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/38/" title="Агент 700" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Агент 700															</span> </a>
                                </li>
                                <li class="menu-item-favorites-more" id="left-menu-hidden-items-block">
                                    <ul class="menu-items-fav-more-block" id="left-menu-hidden-items-list">
                                        <li class="menu-item-separator" id="left-menu-hidden-separator">
                                            <span class="menu-item-sepor-text">Скрытые</span>
                                        </li>
                                        <li id="bx_left_menu_menu_openlines" data-status="hide" data-id="menu_openlines"
                                            data-counter-id="" data-link="/openlines/" data-all-links="/openlines/"
                                            data-type="default" data-delete-perm="N"
                                            data-top-menu-id="top_menu_id_openlines" data-new-page="N"
                                            class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_openlines')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                            <a class="menu-item-link" href="https://cpiv.bitrix24.ru/openlines/" title="Открытые линии"
                                               onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Открытые линии															</span> </a>
                                        </li>
                                        <li id="bx_left_menu_menu_telephony" data-status="hide" data-id="menu_telephony"
                                            data-counter-id="" data-link="/telephony/" data-all-links="/telephony/"
                                            data-type="default" data-delete-perm="N"
                                            data-top-menu-id="top_menu_id_telephony" data-new-page="N"
                                            class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_telephony')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                            <a class="menu-item-link" href="https://cpiv.bitrix24.ru/telephony/" title="Телефония" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Телефония															</span> </a>
                                        </li>
                                        <li id="bx_left_menu_menu_bizproc_sect" data-status="hide"
                                            data-id="menu_bizproc_sect" data-counter-id="bp_tasks"
                                            data-link="/company/personal/bizproc/"
                                            data-all-links="/company/personal/bizproc/,/company/personal/processes/,/bizproc/"
                                            data-type="default" data-delete-perm="N"
                                            data-top-menu-id="top_menu_id_bizproc" data-new-page="N"
                                            class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_bizproc_sect')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                            <a class="menu-item-link" href="https://cpiv.bitrix24.ru/company/personal/bizproc/"
                                               title="Бизнес-процессы" onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Бизнес-процессы															</span><span
                                                    class="menu-item-index-wrap"><span class="menu-item-index"
                                                                                       id="menu-counter-bp_tasks"></span>
								</span>
                                            </a>
                                        </li>
                                        <li id="bx_left_menu_menu_tariff" data-status="hide" data-id="menu_tariff"
                                            data-counter-id="" data-link="/settings/license_all.php"
                                            data-all-links="/settings/" data-type="default" data-delete-perm="N"
                                            data-top-menu-id="top_menu_id_settings" data-new-page="N"
                                            class="menu-item-block">
						<span class="menu-fav-editable-btn menu-favorites-btn"
                              onclick="BX.Bitrix24.LeftMenuClass.openMenuPopup(this, 'menu_tariff')">
								<span class="menu-favorites-btn-icon"></span>
						</span>

						<span class="menu-favorites-btn menu-favorites-draggable"
                              onmousedown="BX.addClass(this.parentNode, 'menu-item-draggable');"
                              onmouseup="BX.removeClass(this.parentNode, 'menu-item-draggable');">
							<span class="menu-fav-draggable-icon"></span>
						</span>

                                            <a class="menu-item-link" href="https://cpiv.bitrix24.ru/settings/license_all.php" title="Мой тариф"
                                               onclick="if (BX.Bitrix24.LeftMenuClass.isEditMode()) return false;
							">
							<span class="menu-item-link-text" data-role="item-text">
								Мой тариф															</span> </a>
                                        </li>
                                        <li class="menu-items-hidden-empty-li" id="left-menu-hidden-empty-item"></li>
                                    </ul>
                                </li>
                            </ul>

                            <div class="menu-favorites-more-btn" id="left-menu-more-btn">
                                <span class="menu-favorites-more-text">Ещё</span>
                                <span class="menu-favorites-more-icon"></span>
                                <span class="menu-item-index menu-item-index-more" id="menu-hidden-counter"
                                      style="display:none">0</span>
                            </div>

                            <div class="menu-favorites-settings-btn" id="menu-favorites-settings-btn">
		<span class="menu-items-title-text" id="left-menu-settings">
			Настроить меню		</span>
                                <span class="menu-favorites-btn-done"
                                      onclick="BX.Bitrix24.LeftMenuClass.applyEditMode();">Сохранить изменения</span>
                            </div>


                        </div>


                        <div class="left-menu-popup-wrapper" id="left-menu-preset-popup" style="display: none">
                            <form method="POST" name="left-menu-preset-form">
                                <div class="left-menu-popup-close"
                                     onclick="BX.PopupWindowManager.getCurrentPopup().close();">
                                    <div class="left-menu-popup-close-item"></div>
                                </div><!--left-menu-popup-close-->
                                <div class="left-menu-popup-header">
                                    <span class="left-menu-popup-header-item">Выберите, с чем вы планируете работать в <span
                                            class="left-menu-popup-header-link">Битрикс24</span> чаще всего</span>
                                </div><!--left-menu-popup-header-->
                                <div class="left-menu-popup-description">
                                    <span class="left-menu-popup-description-item">Это упростит ежедневную работу: выбранный инструмент будет открываться первым, в меню останутся только нужные вам разделы. Поменять меню вы сможете в любое время.</span>
                                </div><!--left-menu-popup-description-->
                                <div class="left-menu-popup-card-container">
                                    <label class="left-menu-popup-card-item js-left-menu-preset-item left-menu-popup-selected"
                                           for="presetTypeSocial">
                                        <div class="left-menu-popup-card-item-title">Компания</div>
                                        <div class="left-menu-popup-card-item-icon-box left-menu-popup-icon-communication">
                                            <div class="left-menu-popup-card-item-icon"></div>
                                        </div>
                                        <div class="left-menu-popup-card-item-info">Общаться и работать вместе</div>
                                        <div class="left-menu-popup-card-item-description">быстро решать любые вопросы
                                        </div>
                                        <input type="radio" name="presetType" value="social" id="presetTypeSocial"
                                               checked="" style="display: none">
                                    </label>


                                    <label class="left-menu-popup-card-item js-left-menu-preset-item"
                                           for="presetTypeTasks">
                                        <div class="left-menu-popup-card-item-title">Задачи и проекты</div>
                                        <div class="left-menu-popup-card-item-icon-box left-menu-popup-icon-task">
                                            <div class="left-menu-popup-card-item-icon"></div>
                                        </div>
                                        <div class="left-menu-popup-card-item-info">Работать вместе и успевать вовремя
                                        </div>
                                        <div class="left-menu-popup-card-item-description">следить за сроками и
                                            контролировать исполнение
                                        </div>
                                        <input type="radio" name="presetType" value="tasks" id="presetTypeTasks"
                                               style="display: none">
                                    </label>

                                    <label class="left-menu-popup-card-item js-left-menu-preset-item "
                                           for="presetTypeSites">
                                        <div class="left-menu-popup-card-item-title">Сайты</div>
                                        <div class="left-menu-popup-card-item-icon-box left-menu-popup-icon-website">
                                            <div class="left-menu-popup-card-item-icon"></div>
                                        </div>
                                        <div class="left-menu-popup-card-item-info">Создавать сайты, которые продают
                                        </div>
                                        <div class="left-menu-popup-card-item-description">самим создавать сайты и
                                            лендинги сразу в CRM
                                        </div>
                                        <input type="radio" name="presetType" value="sites" id="presetTypeSites"
                                               style="display: none">
                                    </label>
                                </div><!--left-menu-popup-card-container-->
                            </form>
                            <div class="left-menu-popup-border"></div>
                        </div><!--left-menu-popup-wrapper-->
                        <div id="bxdynamic_8fELSC_start" style="display:none"></div>
                        <div id="bxdynamic_8fELSC_end" style="display:none"></div>


                        <div class="sitemap-content" id="sitemap-content">
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/">Мои инструменты</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/stream/">Живая лента</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/user/25/tasks/">Задачи и
                                        Проекты</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/sites/">Сайты</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/company/personal/user/25/calendar/">Календарь</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/user/25/disk/path/">Диск</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/company/personal/user/25/photo/">Фотографии</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/user/25/blog/">Сообщения</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/online/">Чат и звонки</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/mail/">Почта</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/company/personal/bizproc/">Бизнес-процессы</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title"
                                   href="https://cpiv.bitrix24.ru/company/personal/user/25/calendar/">Календарь</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/user/25/calendar/">Мой
                                        календарь</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/calendar/">Календарь компании</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/company/personal/user/25/disk/path/">Диск</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/user/25/disk/path/">Мой
                                        диск</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/docs/path/">Общий диск</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/docs/windows.php">Диск для Windows</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/docs/macos.php">Диск для macOS</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/user/25/disk/volume/">Очистка
                                        места</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/marketing/">CRM-маркетинг</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/">Старт</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/letter/">Рассылки</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/ads/">Реклама</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/segment/">Сегменты</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/template/">Мои шаблоны</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/blacklist/">Черный список</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketing/config.php">Настройки</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/workgroups/">Группы</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/287/">Доработка сайтов
                                        uchmag.ru и uchmet.ru</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/578/">Обучение ОУП</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/15/">Программисты ИНТЕРВОЛГИ
                                        (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/934/">ВолГУ</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/1002/">КНОРУС</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/1026/">eobuv.ru
                                        Еврообувь</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/97/">Сайты агентства
                                        ИНТЕРВОЛГА</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/644/">ЕВРАЗ Металл Инпром —
                                        личный кабинет (ЛК ЕМИ)</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/317/">arkont.ru</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/workgroups/group/884/">Клуб клиентов
                                        ЕЭС-Гарант</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/company/personal/bizproc/">Бизнес-процессы</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/bizproc/">Задания
                                        бизнес-процессов</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/personal/processes/">Мои процессы</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/bizproc/processes/">Процессы в ленте</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/bizproc/bizproc/">Все активные</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/marketplace/">Приложения</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/">Все приложения</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/hook/">Вебхуки</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/4/">Производственный
                                        график</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/8/">ДОСКА ЗАДАЧ — все проекты
                                        как на ладони [beta]</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/24/">Потрачено</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/26/">Дни рождения
                                        сотрудников</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/28/">Evernote</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/32/">YouWasted</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/34/">Потрачено+</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/36/">Заявки на внедрение
                                        Битрикс24</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/marketplace/app/38/">Агент 700</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/onec/">1С + CRM Битрикс24</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/onec/">Face-карт</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/onec/tracker/">1С-трекер</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/onec/report/">Отчеты 1С</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/onec/exchange/">Интеграция с CRM</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/company/vis_structure.php">Компания</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/vis_structure.php">Структура
                                        компании</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/">Сотрудники</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/company/lists/">Списки</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/timeman/">Время и отчеты</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/timeman/">График отсутствий</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/timeman/timeman.php">Рабочее время</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/timeman/bitrix24time.php">Bitrix24.Time</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/timeman/work_report.php">Рабочие отчеты</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/timeman/meeting/">Собрания и планерки</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/openlines/">Открытые линии</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/list/">Список</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/openlines/connector/?ID=livechat">Онлайн-чат</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/connector/?ID=viber">Viber</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/openlines/connector/?ID=telegrambot">Telegram</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/openlines/connector/?ID=vkgroup">Вконтакте</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/connector/?ID=facebook">Facebook:
                                        Сообщения</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/connector/?ID=facebookcomments">Facebook:
                                        Комментарии</a>
                                    <a class="sitemap-section-item"
                                       href="https://cpiv.bitrix24.ru/openlines/connector/?ID=instagram">Instagram</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/connector/?ID=network">Битрикс24.Network</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/connector/?ID=botframework">Skype,
                                        Slack, Office365 e-m...</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/openlines/statistics.php">Детальная
                                        статистика</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/telephony/">Телефония</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/telephony/">Баланс и статистика</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/telephony/phones.php">Аппараты</a>
                                </div>
                            </div>
                            <div class="sitemap-section">
                                <a class="sitemap-section-title" href="https://cpiv.bitrix24.ru/settings/license_all.php">Мой тариф</a>
                                <div class="sitemap-section-items">
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/settings/license_all.php">Расширенные
                                        тарифы</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/settings/license_demo.php">Демо-режим</a>
                                    <a class="sitemap-section-item" href="https://cpiv.bitrix24.ru/settings/business_tools.php">Бизнес-инструменты</a>

                                </div>
                            </div>
                            <div class="sitemap-close-link" id="sitemap-close-link" title="Закрыть"></div>
                        </div>


                        <div class="group-panel-content group-panel-content-all" data-filter="all"
                             id="group-panel-content">
                            <div class="group-panel-header">
		<span class="group-panel-header-filters">
			<span class="group-panel-header-filter group-panel-header-filter-all" data-filter="all">Мои группы</span>
						<span class="group-panel-header-filter group-panel-header-filter-extranet"
                              data-filter="extranet">Экстранет</span>
						<span class="group-panel-header-filter group-panel-header-filter-favorites"
                              data-filter="favorites">Избранное					<span
                                class="group-panel-header-filter-counter" id="group-panel-header-filter-counter"></span>
			</span>
		</span>
                            </div>
                            <div class="group-panel-items" id="group-panel-items"><a href="https://cpiv.bitrix24.ru/workgroups/group/253/"
                                                                                     class="group-panel-item group-panel-item-intranet"
                                                                                     data-id="253"><span
                                    class="group-panel-item-text" title="Diving / DGLife24. Спортивная база данных">Diving / DGLife24. Спортивная база данных</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/660/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="660"><span
                                    class="group-panel-item-text"
                                    title="GENEVA и KLEOPATRA">GENEVA и KLEOPATRA</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/688/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="688"><span
                                    class="group-panel-item-text" title="NovaTour b2b">NovaTour b2b</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/169/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="169"><span
                                    class="group-panel-item-text" title="SIEMENS">SIEMENS</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/916/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="916"><span
                                    class="group-panel-item-text" title="anosoff.com">anosoff.com</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/317/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="317"><span
                                    class="group-panel-item-text" title="arkont.ru">arkont.ru</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/1026/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="1026"><span
                                    class="group-panel-item-text"
                                    title="eobuv.ru Еврообувь">eobuv.ru Еврообувь</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/281/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="281"><span
                                    class="group-panel-item-text"
                                    title="mybox - интернет-магазин, потом мобильное приложение">mybox - интернет-магазин, потом мобильное приложение</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/333/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="333"><span
                                    class="group-panel-item-text" title="valdaiclub.com — дискуссионный клуб «Валдай»">valdaiclub.com — дискуссионный клуб «Валдай»</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/219/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="219"><span
                                    class="group-panel-item-text" title="«7соток»">«7соток»</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/263/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="263"><span
                                    class="group-panel-item-text" title="АБМК Итгаз">АБМК Итгаз</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/343/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="343"><span
                                    class="group-panel-item-text"
                                    title="АКЛИС - оптовый ИМ">АКЛИС - оптовый ИМ</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/736/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="736"><span
                                    class="group-panel-item-text" title="АРСС">АРСС</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/61/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="61"><span
                                    class="group-panel-item-text" title="Арбитр (Шелл)">Арбитр (Шелл)</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/906/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="906"><span
                                    class="group-panel-item-text" title="Аструм — стоматология и косметология">Аструм — стоматология и косметология</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/536/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="536"><span
                                    class="group-panel-item-text" title="Беспроводное.рф (обновление besprovodnoe.ru)">Беспроводное.рф (обновление besprovodnoe.ru)</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/768/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="768"><span
                                    class="group-panel-item-text" title="Ветеринарная клиника доктора Чулковой">Ветеринарная клиника доктора Чулковой</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/934/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="934"><span
                                    class="group-panel-item-text" title="ВолГУ">ВолГУ</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/103/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="103"><span
                                    class="group-panel-item-text" title="Волма">Волма</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/928/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="928"><span
                                    class="group-panel-item-text" title="Городской портал (ГО64)">Городской портал (ГО64)</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/554/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="554"><span
                                    class="group-panel-item-text" title="Двери в дом">Двери в дом</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/876/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="876"><span
                                    class="group-panel-item-text" title="Детский фитнес moveplay.ru">Детский фитнес moveplay.ru</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/287/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="287"><span
                                    class="group-panel-item-text" title="Доработка сайтов uchmag.ru и uchmet.ru">Доработка сайтов uchmag.ru и uchmet.ru</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/448/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="448"><span
                                    class="group-panel-item-text" title="ЕВРАЗ — личный кабинет клиентов">ЕВРАЗ — личный кабинет клиентов</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/293/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="293"><span
                                    class="group-panel-item-text"
                                    title="ИКВО — избирательная комиссия Волгоградской области">ИКВО — избирательная комиссия Волгоградской области</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/203/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="203"><span
                                    class="group-panel-item-text" title="Инвест-Недвижимость www.invst.ru">Инвест-Недвижимость www.invst.ru</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/638/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="638"><span
                                    class="group-panel-item-text" title="Канцтоварищи">Канцтоварищи</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/612/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="612"><span
                                    class="group-panel-item-text" title="Карго-Трэвэл auto-th.ru">Карго-Трэвэл auto-th.ru</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/894/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="894"><span
                                    class="group-panel-item-text" title="Катона - сайт и маркетинг">Катона - сайт и маркетинг</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/884/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="884"><span
                                    class="group-panel-item-text" title="Клуб клиентов ЕЭС-Гарант">Клуб клиентов ЕЭС-Гарант</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/291/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="291"><span
                                    class="group-panel-item-text"
                                    title="Круиз - турагентство">Круиз - турагентство</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/860/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="860"><span
                                    class="group-panel-item-text" title="Кумылженская администрация">Кумылженская администрация</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/117/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="117"><span
                                    class="group-panel-item-text" title="Мегалайн-тур">Мегалайн-тур</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/177/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="177"><span
                                    class="group-panel-item-text" title="Микро-бикини">Микро-бикини</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/57/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="57"><span
                                    class="group-panel-item-text"
                                    title="Новые сайты Итгаз itgaz.ru и Тартарини tartarini.su">Новые сайты Итгаз itgaz.ru и Тартарини tartarini.su</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/550/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="550"><span
                                    class="group-panel-item-text" title="ООО «ТД ГраСС» (grass.su)">ООО «ТД ГраСС» (grass.su)</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/221/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="221"><span
                                    class="group-panel-item-text" title="Обмен книгами, Александр Рудской">Обмен книгами, Александр Рудской</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/578/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="578"><span
                                    class="group-panel-item-text" title="Обучение ОУП">Обучение ОУП</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/452/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="452"><span
                                    class="group-panel-item-text" title="Питомники-Шоп — Сопровождение ОИМ">Питомники-Шоп — Сопровождение ОИМ</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/15/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="15"><span
                                    class="group-panel-item-text"
                                    title="Программисты ИНТЕРВОЛГИ (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут">Программисты ИНТЕРВОЛГИ (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/628/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="628"><span
                                    class="group-panel-item-text" title="Проект svetosila.ru (db8.ru)">Проект svetosila.ru (db8.ru)</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/874/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="874"><span
                                    class="group-panel-item-text" title="РЖД. Модернизация сайта">РЖД. Модернизация сайта</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/77/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="77"><span
                                    class="group-panel-item-text" title="Репозиторий кода">Репозиторий кода</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/215/"
                                                                                class="group-panel-item group-panel-item-extranet"
                                                                                data-id="215"><span
                                    class="group-panel-item-text" title="Рособмен">Рособмен</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/97/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="97"><span
                                    class="group-panel-item-text" title="Сайты агентства ИНТЕРВОЛГА">Сайты агентства ИНТЕРВОЛГА</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/249/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="249"><span
                                    class="group-panel-item-text" title="Сантехпрект">Сантехпрект</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/175/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="175"><span
                                    class="group-panel-item-text" title="Саунд 34">Саунд 34</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/85/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="85"><span
                                    class="group-panel-item-text" title="Свод правил и инструкций Интерволги">Свод правил и инструкций Интерволги</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/698/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="698"><span
                                    class="group-panel-item-text" title="СмолМаш">СмолМаш</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/752/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="752"><span
                                    class="group-panel-item-text"
                                    title="СмолМаш (производство)">СмолМаш (производство)</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/740/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="740"><span
                                    class="group-panel-item-text" title="СмолМаш - Интернет-магазин">СмолМаш - Интернет-магазин</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/167/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="167"><span
                                    class="group-panel-item-text" title="ТД Росподшипник">ТД Росподшипник</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/898/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="898"><span
                                    class="group-panel-item-text" title="Телемаксима">Телемаксима</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/59/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="59"><span
                                    class="group-panel-item-text" title="Термокомфорт">Термокомфорт</span><span
                                    class="group-panel-item-star"></span></a><a href="https://cpiv.bitrix24.ru/workgroups/group/189/"
                                                                                class="group-panel-item group-panel-item-intranet"
                                                                                data-id="189"><span
                                    class="group-panel-item-text" title="Техно-Хаус">Техно-Хаус</span><span
                                    class="group-panel-item-star"></span></a></div>
                            <div class="sitemap-close-link group-panel-close-link" id="group-panel-close-link"></div>
                        </div>

                        <div id="bxdynamic_im_start" style="display:none"></div>

                        <div id="bxdynamic_im_end" style="display:none"></div>
                        <div id="feed-up-btn-wrap" class="feed-up-btn-wrap" title="Наверх" onclick="B24.goUp();">
                            <div class="feed-up-btn">
                                <span class="feed-up-text">Наверх</span>
                                <span class="feed-up-btn-icon"></span>
                            </div>
                        </div>
                    </td>
                    <td class="bx-layout-inner-center" id="content-table">
                        <table class="bx-layout-inner-inner-table top-menu-mode">
                            <colgroup>
                                <col class="bx-layout-inner-inner-cont">
                            </colgroup>
                            <tbody>
                            <tr class="bx-layout-inner-inner-top-row">
                                <td class="bx-layout-inner-inner-cont">
                                    <div class="page-header">

                                        <div class="main-buttons">
                                            <div class="main-buttons-inner-container" id="top_menu_id_marketplace"
                                                 style="height: 60px;">
                                                <div class="main-buttons-item"
                                                     id="top_menu_id_marketplace_menu_marketplace" data-disabled="false"
                                                     data-class="" data-onclick="" data-url="/marketplace/"
                                                     data-text="Все приложения" data-id="menu_marketplace"
                                                     data-counter="" data-counter-id="" data-locked="false"
                                                     data-item="{&quot;TEXT&quot;:&quot;\u0412\u0441\u0435 \u043f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u044f&quot;,&quot;URL&quot;:&quot;\/marketplace\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_menu_marketplace&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:&quot;menu_marketplace&quot;,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;false&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:0}"
                                                     data-top-menu-id="top_menu_id_marketplace" title=""
                                                     draggable="true" tabindex="-1" data-link="item0">
                                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/">
                                                        <span class="main-buttons-item-icon"></span><span
                                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Все приложения</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                    </a>
                                                </div><!--main-buttons-item-->
                                                <div class="main-buttons-item"
                                                     id="top_menu_id_marketplace_menu_marketplace_hook"
                                                     data-disabled="false" data-class="" data-onclick=""
                                                     data-url="/marketplace/hook/" data-text="Вебхуки"
                                                     data-id="menu_marketplace_hook" data-counter="" data-counter-id=""
                                                     data-locked="false"
                                                     data-item="{&quot;TEXT&quot;:&quot;\u0412\u0435\u0431\u0445\u0443\u043a\u0438&quot;,&quot;URL&quot;:&quot;\/marketplace\/hook\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_menu_marketplace_hook&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:&quot;menu_marketplace_hook&quot;,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;false&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:1}"
                                                     data-top-menu-id="top_menu_id_marketplace" title=""
                                                     draggable="true" tabindex="-1" data-link="item1">
                                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/hook/">
                                                        <span class="main-buttons-item-icon"></span><span
                                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Вебхуки</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                    </a>
                                                </div><!--main-buttons-item-->
                                                <div class="main-buttons-item" id="top_menu_id_marketplace_521261859"
                                                     data-disabled="false" data-class="" data-onclick=""
                                                     data-url="/marketplace/app/4/" data-text="Производственный график"
                                                     data-id="521261859" data-counter="" data-counter-id=""
                                                     data-locked="false"
                                                     data-item="{&quot;TEXT&quot;:&quot;\u041f\u0440\u043e\u0438\u0437\u0432\u043e\u0434\u0441\u0442\u0432\u0435\u043d\u043d\u044b\u0439 \u0433\u0440\u0430\u0444\u0438\u043a&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/4\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_521261859&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:521261859,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;false&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:2}"
                                                     data-top-menu-id="top_menu_id_marketplace" title=""
                                                     draggable="true" tabindex="-1" data-link="item2">
                                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/4/">
                                                        <span class="main-buttons-item-icon"></span><span
                                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Производственный график</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                    </a>
                                                </div><!--main-buttons-item-->
                                                <div class="main-buttons-item main-buttons-item-more-default main-buttons-item-more main-buttons-item-active"
                                                     id="top_menu_id_marketplace_more_button" draggable="true"
                                                     tabindex="-1" data-link="item11">
                                                    <a href="https://cpiv.bitrix24.ru#" class="main-buttons-item-link">
                                                        <span class="main-buttons-item-icon"></span><span
                                                            class="main-buttons-item-text">Еще</span>
                                                        <span class="main-buttons-item-counter"></span>
                                                    </a>
                                                </div>
                                                <div class="main-buttons-item" id="top_menu_id_marketplace_3013909551"
                                                     data-disabled="false" data-class="" data-onclick=""
                                                     data-url="/marketplace/app/8/"
                                                     data-text="ДОСКА ЗАДАЧ — все проекты как на ладони [beta]"
                                                     data-id="3013909551" data-counter="" data-counter-id=""
                                                     data-locked="false"
                                                     data-item="{&quot;TEXT&quot;:&quot;\u0414\u041e\u0421\u041a\u0410 \u0417\u0410\u0414\u0410\u0427 \u2014 \u0432\u0441\u0435 \u043f\u0440\u043e\u0435\u043a\u0442\u044b \u043a\u0430\u043a \u043d\u0430 \u043b\u0430\u0434\u043e\u043d\u0438 [beta]&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/8\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_3013909551&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:3013909551,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;false&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:4}"
                                                     data-top-menu-id="top_menu_id_marketplace" title=""
                                                     draggable="true" tabindex="-1" data-link="item3">
                                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/8/">
                                                        <span class="main-buttons-item-icon"></span><span
                                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">ДОСКА ЗАДАЧ — все проекты как на ладони [beta]</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                    </a>
                                                </div><!--main-buttons-item-->
                                                <div class="main-buttons-item " id="top_menu_id_marketplace_1980761437"
                                                     data-disabled="false" data-class="" data-onclick=""
                                                     data-url="/marketplace/app/24/" data-text="Потрачено"
                                                     data-id="1980761437" data-counter="" data-counter-id=""
                                                     data-locked="false"
                                                     data-item="{&quot;TEXT&quot;:&quot;\u041f\u043e\u0442\u0440\u0430\u0447\u0435\u043d\u043e&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/24\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_1980761437&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:1980761437,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;false&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:6}"
                                                     data-top-menu-id="top_menu_id_marketplace" title=""
                                                     draggable="true" tabindex="-1" data-link="item4">
                                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/24/">
                                                        <span class="main-buttons-item-icon"></span><span
                                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Потрачено</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                    </a>
                                                </div><!--main-buttons-item-->
                                                <div class="main-buttons-item " id="top_menu_id_marketplace_3668265553"
                                                     data-disabled="" false
                                                ""="" data-class="" data-onclick="" data-url="/marketplace/app/28/"
                                                data-text="Evernote" data-id="3668265553" data-counter=""
                                                data-counter-id="" data-locked="false" data-item="{&quot;TEXT&quot;:&quot;Evernote&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/28\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_3668265553&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:3668265553,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;\u0022false\u0022&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:6}"
                                                data-top-menu-id="top_menu_id_marketplace" title="" draggable="true"
                                                tabindex="-1" data-link="item5">
                                                <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/28/">
                                                    <span class="main-buttons-item-icon"></span><span
                                                        class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Evernote</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                </a>
                                            </div><!--main-buttons-item-->
                                            <div class="main-buttons-item " id="top_menu_id_marketplace_1143367647"
                                                 data-disabled="false" data-class="" data-onclick=""
                                                 data-url="/marketplace/app/26/" data-text="Дни рождения сотрудников"
                                                 data-id="1143367647" data-counter="" data-counter-id=""
                                                 data-locked="false"
                                                 data-item="{&quot;TEXT&quot;:&quot;\u0414\u043d\u0438 \u0440\u043e\u0436\u0434\u0435\u043d\u0438\u044f \u0441\u043e\u0442\u0440\u0443\u0434\u043d\u0438\u043a\u043e\u0432&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/26\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_1143367647&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:1143367647,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;false&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:7}"
                                                 data-top-menu-id="top_menu_id_marketplace" title="" draggable="true"
                                                 tabindex="-1" data-link="item6">
                                                <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/26/">
                                                    <span class="main-buttons-item-icon"></span><span
                                                        class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Дни рождения сотрудников</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                                </a>
                                            </div><!--main-buttons-item-->
                                            <div class="main-buttons-item " id="top_menu_id_marketplace_562612460"
                                                 data-disabled="" false
                                            ""="" data-class="" data-onclick="" data-url="/marketplace/app/32/"
                                            data-text="YouWasted" data-id="562612460" data-counter="" data-counter-id=""
                                            data-locked="false" data-item="{&quot;TEXT&quot;:&quot;YouWasted&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/32\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_562612460&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:562612460,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;\u0022false\u0022&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:7}"
                                            data-top-menu-id="top_menu_id_marketplace" title="" draggable="true"
                                            tabindex="-1" data-link="item7">
                                            <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/32/">
                                                <span class="main-buttons-item-icon"></span><span
                                                    class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">YouWasted</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                            </a>
                                        </div><!--main-buttons-item-->
                                        <div class="main-buttons-item  main-buttons-item-active"
                                             id="top_menu_id_marketplace_2010279786" data-disabled="" false
                                        ""="" data-class="" data-onclick="" data-url="/marketplace/app/34/"
                                        data-text="Потрачено+" data-id="2010279786" data-counter="" data-counter-id=""
                                        data-locked="false" data-item="{&quot;TEXT&quot;:&quot;\u041f\u043e\u0442\u0440\u0430\u0447\u0435\u043d\u043e+&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/34\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_2010279786&quot;,&quot;IS_ACTIVE&quot;:true,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:2010279786,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;\u0022false\u0022&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:8}"
                                        data-top-menu-id="top_menu_id_marketplace" title="" draggable="true"
                                        tabindex="-1" data-link="item8">
                                        <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/34/">
                                            <span class="main-buttons-item-icon"></span><span
                                                class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Потрачено+</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                        </a>
                                    </div><!--main-buttons-item-->
                                    <div class="main-buttons-item " id="top_menu_id_marketplace_1172573672"
                                         data-disabled="" false
                                    ""="" data-class="" data-onclick="" data-url="/marketplace/app/36/"
                                    data-text="Заявки на внедрение Битрикс24" data-id="1172573672" data-counter=""
                                    data-counter-id="" data-locked="false" data-item="{&quot;TEXT&quot;:&quot;\u0417\u0430\u044f\u0432\u043a\u0438
                                    \u043d\u0430 \u0432\u043d\u0435\u0434\u0440\u0435\u043d\u0438\u0435
                                    \u0411\u0438\u0442\u0440\u0438\u043a\u044124&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/36\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_1172573672&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:1172573672,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;\u0022false\u0022&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:9}"
                                    data-top-menu-id="top_menu_id_marketplace" title="" draggable="true" tabindex="-1"
                                    data-link="item9">
                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/36/">
                                        <span class="main-buttons-item-icon"></span><span
                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Заявки на внедрение Битрикс24</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                    </a>
                                    </div><!--main-buttons-item-->
                                    <div class="main-buttons-item " id="top_menu_id_marketplace_3680968806"
                                         data-disabled="" false
                                    ""="" data-class="" data-onclick="" data-url="/marketplace/app/38/" data-text="Агент
                                    700" data-id="3680968806" data-counter="" data-counter-id="" data-locked="false"
                                    data-item="{&quot;TEXT&quot;:&quot;\u0410\u0433\u0435\u043d\u0442 700&quot;,&quot;URL&quot;:&quot;\/marketplace\/app\/38\/&quot;,&quot;ID&quot;:&quot;top_menu_id_marketplace_3680968806&quot;,&quot;IS_ACTIVE&quot;:false,&quot;IS_LOCKED&quot;:&quot;false&quot;,&quot;HTML&quot;:&quot;&quot;,&quot;CLASS&quot;:&quot;&quot;,&quot;CLASS_SUBMENU_ITEM&quot;:&quot;&quot;,&quot;DATA_ID&quot;:3680968806,&quot;MAX_COUNTER_SIZE&quot;:99,&quot;ON_CLICK&quot;:&quot;&quot;,&quot;IS_DISABLED&quot;:&quot;\u0022false\u0022&quot;,&quot;SUB_LINK&quot;:false,&quot;SORT&quot;:10}"
                                    data-top-menu-id="top_menu_id_marketplace" title="" draggable="true" tabindex="-1"
                                    data-link="item10">
                                    <a class="main-buttons-item-link" href="https://cpiv.bitrix24.ru/marketplace/app/38/">
                                        <span class="main-buttons-item-icon"></span><span
                                            class="main-buttons-item-text">
							<span class="main-buttons-item-edit-button"></span>
							<span class="main-buttons-item-text-title">Агент 700</span>
							<span class="main-buttons-item-drag-button"></span>
							<span class="main-buttons-item-text-marker"></span>
						</span><span class="main-buttons-item-counter"></span>
                                    </a>
                                    </div><!--main-buttons-item-->
                                    <!--main-buttons-item-->
                                    </div><!--main-buttons-inner-container-->
                                    <iframe height="100%" width="100%"
                                            id="maininterfacebuttons-tmp-frame-top_menu_id_marketplace"
                                            name="maininterfacebuttonstmpframe-top_menu_id_marketplace"
                                            style="position: absolute; z-index: -1; opacity: 0;"></iframe>
                                    </div><!--main-buttons-->


                                    <div class="pagetitle-wrap">
                                        <div class="pagetitle-inner-container">
                                            <div class="pagetitle-menu pagetitle-container pagetitle-last-item-in-a-row"
                                                 id="pagetitle-menu">

                                            </div>
                                            <div class="pagetitle">
                                                <span id="pagetitle" class="pagetitle-item">${json.title}</span>
                                                <span class="pagetitle-star" id="pagetitle-star"
                                                      title="Добавить текущую страницу в левое меню"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pagetitle-below">
                                    </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="bx-layout-inner-inner-cont">

                                    <div id="workarea">
                                        <div id="sidebar"></div>
                                        <div id="workarea-content">
${content} 
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="bx-layout-inner-left" id="layout-left-column-bottom"></td>
                    <td class="bx-layout-inner-center">
                        <div id="footer">
							<span id="copyright">
																										<span class="bx-lang-btn ru"
                                                                                                              id="bx-lang-btn"
                                                                                                              onclick="B24.openLanguagePopup(this)"
                                                                                                              data-langs="{&quot;en&quot;: &quot;English&quot;, &quot;de&quot;: &quot;Deutsch&quot;, &quot;la&quot;: &quot;Español&quot;, &quot;br&quot;: &quot;Português (BR)&quot;, &quot;fr&quot;: &quot;Français, beta&quot;, &quot;pl&quot;: &quot;Polski, beta&quot;, &quot;ru&quot;: &quot;Русский&quot;, &quot;ua&quot;: &quot;Українська&quot;, &quot;tr&quot;: &quot;Türkçe, beta&quot;, &quot;tc&quot;: &quot;中文（繁體）&quot;, &quot;sc&quot;: &quot;中文（简体）&quot;}">
										<span class="bx-lang-btn-icon"></span>
									</span>
									<a id="bitrix24-logo" target="_blank" class="bitrix24-logo-ru"
                                       href="https://cpiv.bitrix24.ruhttps://www.bitrix24.ru"></a>
																<span class="bitrix24-copyright">© «Битрикс», 2018</span>
																									<a href="https://cpiv.bitrix24.rujavascript:void(0)"
                                                                                                       onclick="showPartnerForm({'MESS':{'BX24_PARTNER_TITLE':'Персональная поддержка','BX24_CLOSE_BUTTON':'Закрыть','BX24_LOADING':'загрузка'}}); return false;"
                                                                                                       class="footer-discuss-link">Связаться с партнером</a>

																	<span class="footer-themes-link"
                                                                          id="footer-themes-link"
                                                                          onclick="BX.Intranet.Bitrix24.ThemePicker.Singleton.showDialog()">Тема оформления</span>
															</span>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>

<div class="bx-im-bar " id="bx-im-bar" style="top: 63px;">
    <div class="bx-im-informer bx-im-border-b">
        <div id="bx-im-bar-notify" class="bx-im-informer-icon" title="">
            <div class="bx-im-informer-num"></div>
        </div>
    </div>
    <div id="bx-im-bar-search" class="bx-im-search bx-im-border-b" title="Перейти к поиску сотрудников и чатов">
        <div class="bx-im-informer-num"></div>
    </div>
    <div class="bx-im-users-wrap">
        <div class="bx-im-scroll-wrap" id="bx-im-external-recent-list">
            <div class="bx-messenger-recent-group"><span class="bx-messenger-recent-group-title">Сб, 17 Марта</span>
            </div>
            <span data-userid="chat6208"
                  data-name="Workgroup: &quot;Программисты ИНТЕРВОЛГИ (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут&quot;"
                  data-status="guest" data-avatar="/bitrix/js/im/images/blank.gif" data-userischat="true"
                  data-ispinned="undefined"
                  class="bx-messenger-cl-item  bx-messenger-cl-id-chat6208 bx-messenger-cl-item-chat   bx-messenger-cl-item-chat-chat  "><span
                    class="bx-messenger-cl-count"></span><span
                    title="Workgroup: &quot;Программисты ИНТЕРВОЛГИ (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут&quot;"
                    class="bx-messenger-cl-avatar bx-messenger-cl-avatar-chat  bx-messenger-cl-avatar-status-hide"><img
                    class="bx-messenger-cl-avatar-img bx-messenger-cl-avatar-img-default"
                    src="https://cpiv.bitrix24.ru/bitrix/js/im/images/blank.gif" style="background-color: #6fc8e5"><span
                    class="bx-messenger-cl-status"></span></span><span class="bx-messenger-cl-user"><div
                    class="bx-messenger-cl-user-title"
                    title="Workgroup: &quot;Программисты ИНТЕРВОЛГИ (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут&quot;">Workgroup: "Программисты ИНТЕРВОЛГИ (это НЕ отдел поддержки) отсутствия и опоздания -- НЕ тут"</div><div
                    class="bx-messenger-cl-user-desc">Дмитрий Павлов такое могло произойти, если есть несколько песочниц с одной БД при удалении одной из песочниц</div></span></span>
            <div class="bx-messenger-recent-group"><span class="bx-messenger-recent-group-title">Чт, 15 Марта</span>
            </div>
            <span data-userid="155" data-name="Наталья Швед" data-status="dnd"
                  data-avatar="https://bitrix2.cdnvideo.ru/b117697/resize_cache/97512/45a41af90f4a8f8ae94152f60a6ec694/main/89a/89a7f78d860a4d3d490cb04d219110dd/2.jpg?h=cpiv.bitrix24.ru"
                  data-ispinned="undefined"
                  class="bx-messenger-cl-item  bx-messenger-cl-id-155 bx-messenger-cl-status-dnd   "><span
                    class="bx-messenger-cl-count"></span><span title="Наталья Швед"
                                                               class="bx-messenger-cl-avatar  "><img
                    class="bx-messenger-cl-avatar-img"
                    src="https://bitrix2.cdnvideo.ru/b117697/resize_cache/97512/45a41af90f4a8f8ae94152f60a6ec694/main/89a/89a7f78d860a4d3d490cb04d219110dd/2.jpg?h=cpiv.bitrix24.ru"><span
                    class="bx-messenger-cl-status"></span></span><span class="bx-messenger-cl-user"><div
                    class="bx-messenger-cl-user-title" title="Наталья Швед">Наталья Швед</div><div
                    class="bx-messenger-cl-user-desc">спасибо!</div></span></span>
            <div class="bx-messenger-recent-group"><span class="bx-messenger-recent-group-title">Пн, 5 Марта</span>
            </div>
            <span data-userid="chat1228" data-name="Общий чат" data-status="guest"
                  data-avatar="/bitrix/js/im/images/blank.gif" data-userischat="true" data-ispinned="undefined"
                  class="bx-messenger-cl-item  bx-messenger-cl-id-chat1228 bx-messenger-cl-item-chat   bx-messenger-cl-item-chat-open bx-messenger-cl-item-chat-general "><span
                    class="bx-messenger-cl-count"></span><span title="Общий чат"
                                                               class="bx-messenger-cl-avatar bx-messenger-cl-avatar-open  bx-messenger-cl-item-chat-general bx-messenger-cl-avatar-status-hide"><img
                    class="bx-messenger-cl-avatar-img bx-messenger-cl-avatar-img-default"
                    src="https://cpiv.bitrix24.ru/bitrix/js/im/images/blank.gif" style="background-color: #29619b"><span
                    class="bx-messenger-cl-status"></span></span><span class="bx-messenger-cl-user"><div
                    class="bx-messenger-cl-user-title" title="Общий чат">Общий чат</div><div
                    class="bx-messenger-cl-user-desc">dmitriev@intervolga.ru принят на работу</div></span></span>
            <div class="bx-messenger-recent-group"><span class="bx-messenger-recent-group-title">Ср, 28 Февраля</span>
            </div>
            <span data-userid="180" data-name="Андрей Подцветов" data-status="online"
                  data-avatar="https://bitrix2.cdnvideo.ru/b117697/resize_cache/72252/45a41af90f4a8f8ae94152f60a6ec694/main/2c4/2c45ec34e7aa8c2dee54f671821edf05/z_b03a0171.jpg?h=cpiv.bitrix24.ru"
                  data-ispinned="undefined"
                  class="bx-messenger-cl-item  bx-messenger-cl-id-180 bx-messenger-cl-status-online   "><span
                    class="bx-messenger-cl-count"></span><span title="Андрей Подцветов"
                                                               class="bx-messenger-cl-avatar  "><img
                    class="bx-messenger-cl-avatar-img"
                    src="https://bitrix2.cdnvideo.ru/b117697/resize_cache/72252/45a41af90f4a8f8ae94152f60a6ec694/main/2c4/2c45ec34e7aa8c2dee54f671821edf05/z_b03a0171.jpg?h=cpiv.bitrix24.ru"><span
                    class="bx-messenger-cl-status"></span></span><span class="bx-messenger-cl-user"><div
                    class="bx-messenger-cl-user-title" title="Андрей Подцветов">Андрей Подцветов</div><div
                    class="bx-messenger-cl-user-desc"><span class="bx-messenger-cl-user-reply"></span>Потрачено более 80% времени на тикет https://youtrack.ivsupport.ru/issue/LSI-467</div></span></span>
            <div class="bx-messenger-recent-group"><span class="bx-messenger-recent-group-title">Ср, 21 Февраля</span>
            </div>
            <span data-userid="2714" data-name="Игорь Суханов" data-status="online"
                  data-avatar="/bitrix/js/im/images/blank.gif" data-ispinned="undefined"
                  class="bx-messenger-cl-item  bx-messenger-cl-id-2714 bx-messenger-cl-status-online   "><span
                    class="bx-messenger-cl-count"></span><span title="Игорь Суханов"
                                                               class="bx-messenger-cl-avatar  "><img
                    class="bx-messenger-cl-avatar-img bx-messenger-cl-avatar-img-default"
                    src="https://cpiv.bitrix24.ru/bitrix/js/im/images/blank.gif" style="background-color: "><span
                    class="bx-messenger-cl-status"></span></span><span class="bx-messenger-cl-user"><div
                    class="bx-messenger-cl-user-title" title="Игорь Суханов">Игорь Суханов</div><div
                    class="bx-messenger-cl-user-desc">Задача №57344( https://cpiv.bitrix24.ru/company/personal/user/0/tasks/task/view/57344/ ) не принадлежит ни одной из рабочих групп.</div></span></span><span
                data-userid="chat11006" data-name="Красный чат №76" data-status="guest"
                data-avatar="/bitrix/js/im/images/blank.gif" data-userischat="true" data-ispinned="undefined"
                class="bx-messenger-cl-item  bx-messenger-cl-id-chat11006 bx-messenger-cl-item-chat   bx-messenger-cl-item-chat-chat  "><span
                class="bx-messenger-cl-count"></span><span title="Красный чат №76"
                                                           class="bx-messenger-cl-avatar bx-messenger-cl-avatar-chat  bx-messenger-cl-avatar-status-hide"><img
                class="bx-messenger-cl-avatar-img bx-messenger-cl-avatar-img-default"
                src="https://cpiv.bitrix24.ru/bitrix/js/im/images/blank.gif" style="background-color: #df532d"><span
                class="bx-messenger-cl-status"></span></span><span class="bx-messenger-cl-user"><div
                class="bx-messenger-cl-user-title" title="Красный чат №76">Красный чат №76</div><div
                class="bx-messenger-cl-user-desc">Спасибо</div></span></span></div>
    </div>
    <div class="bx-im-bottom-block" id="bx-im-bottom-block">
        <div id="bx-im-btn-call" class="bx-im-btn-wrap bx-im-btn-call" title="Совершить звонок">
            <div class="bx-im-btn"></div>
        </div>
    </div>
</div>


<div id="bxdynamic_bx-pull-start_start" style="display:none"></div>
<div id="bxdynamic_bx-pull-start_end" style="display:none"></div>
<div id="bxdynamic_g3dcTg_start" style="display:none"></div>

<div id="bxdynamic_g3dcTg_end" style="display:none"></div>
<div id="bxdynamic_otp-info_start" style="display:none"></div>
<div id="bxdynamic_otp-info_end" style="display:none"></div>
<div id="bxdynamic_ziLaXt_start" style="display:none"></div>


<div id="bxdynamic_ziLaXt_end" style="display:none"></div>


<!--33b9dbac1893ab68cb9af58d315846e5-->
<div id="tasks-iframe-popup" class="popup-window popup-window-content-no-paddings popup-window-with-titlebar"
     style="z-index: 1000; position: absolute; display: none; top: 0px; left: 0px;">
    <div class="popup-window-titlebar" id="popup-window-titlebar-tasks-iframe-popup"></div>
    <div id="popup-window-content-tasks-iframe-popup" class="popup-window-content">
        <div id="tasks-iframe-wrap" class="tasks-iframe-wrap loading fixedHeight" style="display: block;">
            <iframe scrolling="no" frameborder="0"></iframe>
        </div>
    </div>
    <span class="popup-window-close-icon popup-window-titlebar-close-icon"></span></div>
<div class="popup-window-overlay" id="popup-window-overlay-tasks-iframe-popup"
     style="z-index: 999; width: 1409px; height: 900px;"></div>
<div id="tasks-popup-close" title="Закрыть окно" class="hidden"><span></span></div>
</body>
</html>
        
        `
    });
}