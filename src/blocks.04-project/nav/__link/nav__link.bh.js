module.exports = function (bh) {
    bh.match('nav__link', function (ctx, json) {
        ctx
            .tag('a')
            .cls([
                'nav-item',
                'nav-link',
                json.active && 'active'
            ].join(' '))
            .attrs({
                'data-toggle': 'tab',
                'aria-selected': 'true',
                id: ctx.generateId(),
                href: json.href || '#',
            })
    })
}