module.exports = {
  block: 'environment', title: 'Юрий Петрович Камнев, 192 837 руб., Волгоград, КПК "Апатиты - Кредит"', content: [
    {block: 'section', content: [
      require('./common/form/form-info.bemjson')
    ]},
    {block: 'section', content: [
      require('./common/tables/table-case.bemjson')
    ]},
    {block: 'nav', cls: 'nav-tabs', content: [
      {elem: 'link', href: '#TAB_1', content: 'Выписка по должнику'},
      {elem: 'link', active: true, href: '#TAB_2', content: 'Претенциозно-судебная работа'},
      {elem: 'link', href: '#TAB_3', content: 'Кейсы взыскания'},
      {elem: 'link', href: '#TAB_4', content: 'ССП'}
    ]},
    {block: 'tab-content', content: [
      {block: 'tab-pane', cls: 'fade', attrs: {id: 'TAB_1'}, content: [
        {block: 'section', content: [
          'Выписка по должнику'
        ]}
      ]},
      {block: 'tab-pane', cls: 'fade show active', attrs: {id: 'TAB_2'}, content: [
        {block: 'section', content: [
          {block: 'nav', cls: 'nav-pills mbm', content: [
            {elem: 'link', href: '#TAB_5', content: 'Работа от 01.12.2017'},
            {elem: 'link', href: '#TAB_6', content: 'Работа от 01.12.2017'},
            {elem: 'link', active: true, href: '#TAB_7', content: 'Работа от 01.12.2017'},
            {elem: 'link', href: '#TAB_8', content: 'Работа от 01.12.2017'}
          ]},
          {block: 'tab-content mbl', content: [
            {block: 'tab-pane', cls: 'fade', attrs: {id: 'TAB_5'}, content: [
              '1'
            ]},
            {block: 'tab-pane', cls: 'fade', attrs: {id: 'TAB_6'}, content: [
              '2'
            ]},
            {block: 'tab-pane', cls: 'fade  show active', attrs: {id: 'TAB_7'}, content: [
              require('./common/form/form-list.bemjson')
            ]},
            {block: 'tab-pane', cls: 'fade', attrs: {id: 'TAB_8'}, content: [
              '4'
            ]}
          ]}
        ]}
      ]},
      {block: 'tab-pane', cls: 'fade', attrs: {id: 'TAB_3'}, content: [
        {block: 'section', content: [
          require('./common/card/card-list.bemjson')
        ]}
      ]},
      {block: 'tab-pane', cls: 'fade', attrs: {id: 'TAB_4'}, content: [
        {block: 'section', content: [
          'ССП'
        ]}
      ]}
    ]},
    {block: 'modal', cls: 'fade', attrs: {id: 'MODAL_1'}, content: [
      {elem: 'header', cls: 'text-center', content: [
        'Судебный акт от 01.04.2018',
        {elem: 'close'}
      ]},
      {elem: 'body', content: [
        require('./common/form/form-list.bemjson')
      ]}
    ]}
  ]
};
