module.exports = [
    {block: 'form-section', content: [
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Документ:'},
            {block: 'col', cls: 'col-8', content: [
                {block: 'a', cls: 'link-solid', content: 'Постановление о наложении ареста на имущество'}
            ]}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Идентификатор:'},
            {block: 'col', cls: 'col-8', content: '234234-65756756'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Трекер:'},
            {block: 'col', cls: 'col-8', content: '234234-65756756'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Ответ:'},
            {block: 'col', cls: 'col-8', content: 'Текст полученного ответа текст полученного ответа текст полученного ответа текст полученного ответа'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Идентификатор:'},
            {block: 'col', cls: 'col-8', content: '01.01.2018'}
        ]}
    ]}
];