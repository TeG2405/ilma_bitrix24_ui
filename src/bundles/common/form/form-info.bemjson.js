module.exports = [
    {block: 'row', content: [
        {block: 'col', cls: 'col-6', content: [
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', cls: 'col-4 text-muted', content: 'Срок работы с делом:'},
                {block: 'col', cls: 'col-8', content: '180 дней'}
            ]},
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', cls: 'col-4 text-muted', content: 'Последний платеж:'},
                {block: 'col', cls: 'col-8', content: '12 000 руб., 27.10.2017'}
            ]},
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', content: [
                    {block: 'a', content: 'Посмотреть историю платежей'}
                ]}
            ]}
        ]},
        {block: 'col', cls: 'col-6', content: [
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', cls: 'col-4 text-muted', content: 'Приоритетное дело:'},
                {block: 'col', cls: 'col-8', content: [
                    {attrs: {style: 'line-height: 0'}, content: [
                        {block: 'pagetitle-star', cls: 'pagetitle-star-active'},
                        {block: 'pagetitle-star'},
                    ]}
                ]}
            ]},
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', cls: 'col-4 text-muted', content: 'Стадия:'},
                {block: 'col', cls: 'col-8', content: 'Судебное дело'}
            ]},
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', content: [
                    {block: 'a', content: 'Посмотреть историю изменений дела'}
                ]}
            ]}
        ]}
    ]},
    {tag: 'hr'},
    {block: 'row', content: [
        {block: 'col', cls: 'col-6', content: [
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', cls: 'col-4 text-muted', content: 'Дата ответа ОХ:'},
                {block: 'col', cls: 'col-8', content: '01.01.2018'}
            ]}
        ]},
        {block: 'col', cls: 'col-6', content: [
            {block: 'form-group', cls: 'row', content: [
                {block: 'col', cls: 'col-4 text-muted', content: 'Дата запроса:'},
                {block: 'col', cls: 'col-4', content: '01.01.2018'},
                {block: 'col', cls: 'col-4', content: [
                    {block: 'a', cls: 'link-dash', attrs: {'data-toggle': 'modal', href: '#MODAL_1'}, content: 'Детализация кейсов'}
                ]}
            ]}
        ]}
    ]}
];