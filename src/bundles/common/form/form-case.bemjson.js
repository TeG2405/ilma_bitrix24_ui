module.exports = [
    {block: 'form-section', content: [
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'На кого:'},
            {block: 'col', cls: 'col-8', content: [
                {block: 'a', cls: 'link-solid', content: 'Петров Пётр Петрович'},
                ' (Поручитель)'
            ]}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'ИД:'},
            {block: 'col', cls: 'col-8', content: [
                {block: 'a', cls: 'link-solid', content: 'Исполнительный лист от 01.01.2018'}
            ]}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Дата:'},
            {block: 'col', cls: 'col-8', content: '01.01.2018'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Контрольная дата:'},
            {block: 'col', cls: 'col-8', content: '01.01.2018'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Действие:'},
            {block: 'col', cls: 'col-8', content: 'Обработка ЦХ'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Комментарии:'},
            {block: 'col', cls: 'col-8', content: [
                {block: 'a', cls: 'link-solid', content: '1 комментарий'}
            ]}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Ответственный:'},
            {block: 'col', cls: 'col-8', content: [
                {block: 'a', cls: 'link-solid', content: 'Иванов Иван Иванович'}
            ]}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Задание:'},
            {block: 'col', cls: 'col-8', content: 'Краткий текст задания, задания краткий текст краткий, задания краткий текст краткий, задания краткий текст краткий'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Задачи:'},
            {block: 'col', cls: 'col-8', content: [
                {block: 'a', cls: 'link-solid', content: 'Открыть в новой вкладке'}
            ]}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Завершено:'},
            {block: 'col', cls: 'col-8', content: 'Да'}
        ]},
        {block: 'form-group', cls: 'row', content: [
            {block: 'col', cls: 'col-4 text-muted text-right', content: 'Оценка'},
            {block: 'col', cls: 'col-8', content: 'Удоволетворительно'}
        ]},
    ]}
];