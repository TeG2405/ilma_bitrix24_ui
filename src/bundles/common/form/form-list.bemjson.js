module.exports = [
    {block: 'row', content: [
        {block: 'col', content: [
            {block: 'row', content: [
                {block: 'col', content: [
                    {block: 'form-title', content: [
                        'Судебная работа',
                        {elem: 'switch', cls: 'mlm'}
                    ]},
                ]}
            ]},
            require('./form-case.bemjson.js'),
        ]},
        {block: 'col', content: [
            {block: 'form-section', content: [
                {block: 'row', content: [
                    {block: 'col', content: [
                        {block: 'form-title', content: [
                            'Суммы',
                            {elem: 'switch', cls: 'mlm'}
                        ]}
                    ]}
                ]},
                {block: 'form-section', content: [
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма задолженности:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма ОД:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма процентов:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма ЧВ:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма пени ЧВ:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма пени:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма ЮУ:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Сумма ГП:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'form-control', content: '28 000 000 руб.'}
                        ]}
                    ]},
                ]}
            ]},
            {block: 'form-section', content: [
                {block: 'row', content: [
                    {block: 'col', content: [
                        {block: 'form-title', content: [
                            'Суд',
                            {elem: 'switch', cls: 'mlm'}
                        ]}
                    ]}
                ]},
                {block: 'form-section', content: [
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Суд:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'a', cls: 'link-solid', content: 'Название суда'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Судья:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'a', cls: 'link-solid', content: 'Иванов Иван Иванович'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Судебные определения:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'a', cls: 'link-solid', content: '5 определений'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Акт от 01.01.2018:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'a', cls: 'link-solid', content: 'Открыть'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Акт от 01.02.2018:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'a', cls: 'link-solid', content: 'Открыть'}
                        ]}
                    ]},
                    {block: 'form-group', cls: 'row', content: [
                        {block: 'col', cls: 'col-4 text-muted text-right', content: 'Акт от 01.03.2018:'},
                        {block: 'col', cls: 'col-8', content: [
                            {block: 'a', cls: 'link-solid', content: 'Открыть'}
                        ]}
                    ]},
                ]}
            ]}
        ]}
    ]}
];