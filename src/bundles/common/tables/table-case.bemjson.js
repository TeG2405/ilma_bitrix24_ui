module.exports = [
  {block: 'filter-list', content: [
    {elem: 'li', content: {block: 'span', content: 'Показать кейсы:'}},
    {elem: 'li', content: {block: 'a', cls: 'active', content: 'Все'}},
    {elem: 'li', content: {block: 'a', content: 'Активные'}},
    {elem: 'li', content: {block: 'a', content: 'Завершенные'}}
  ]}, 
  {block: 'table', content: [
    {elem: 'thead', content: [
      {elem: 'tr', content: [
        {elem: 'th', content: 'Кейсы'},
        {elem: 'th', attrs: {colspan: 2}, content: 'На кого'},
        {elem: 'th', content: 'Дата отправки'},
        {elem: 'th', content: 'Ответы ФССП'},
        {elem: 'th', content: 'Дата ответа'},
        {elem: 'th', content: 'Полезная инфо?'},
        {elem: 'th', content: 'Проработано'},
      ]}
    ]},
    {elem: 'tbody', content: [
      {elem: 'tr', content: [
        {elem: 'td', content: [
          {block: 'form-check', content: 'ЦХ фиктивные сделки'}
        ]},
        {elem: 'td', content: [
          {block: 'a', cls: 'link-dash', content: 'Иванов И.И.'}
        ]},
        {elem: 'td', content: [
          {block: 'span', cls: 'text-muted', content: 'Поручитель'}
        ]},
        {elem: 'td', content: '01.01.2018'},
        {elem: 'td', content: [
          {block: 'form-check', checked: true, disabled: true, content: 'ФНС'}
        ]},
        {elem: 'td', content: '01.01.2018'},
        {elem: 'td', content: 'Нет'},
        {elem: 'td', content: 'Нет'}
      ]},
      {elem: 'tr', content: [
        {elem: 'td', content: [
          {block: 'form-check', content: 'ЦХ фиктивные сделки'}
        ]},
        {elem: 'td', content: [
          {block: 'a', cls: 'link-dash', content: 'Иванов И.И.'}
        ]},
        {elem: 'td', content: [
          {block: 'span', cls: 'text-muted', content: 'Поручитель'}
        ]},
        {elem: 'td', content: '01.01.2018'},
        {elem: 'td', content: [
          {block: 'form-check', content: 'ФНС'}
        ]},
        {elem: 'td', content: '01.01.2018'},
        {elem: 'td', content: 'Нет'},
        {elem: 'td', content: 'Нет'}
      ]},
      {elem: 'tr', content: [
        {elem: 'td', content: [
          {block: 'form-check', disabled: true, content: 'ЦХ фиктивные сделки'}
        ]},
        {elem: 'td', content: [
          {block: 'a', cls: 'link-dash', content: 'Иванов И.И.'}
        ]},
        {elem: 'td', content: [
          {block: 'span', cls: 'text-muted', content: 'Поручитель'}
        ]},
        {elem: 'td', content: '01.01.2018'},
        {elem: 'td', content: [
          {block: 'form-check', content: 'ФНС'}
        ]},
        {elem: 'td', content: '01.01.2018'},
        {elem: 'td', content: 'Нет'},
        {elem: 'td', content: 'Нет'}
      ]}
    ]}
  ]}
];
