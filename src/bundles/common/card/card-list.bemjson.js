module.exports = [
    {block: 'card', content: [
        {elem: 'header', cls: 'collapsed', tag: 'a', attrs: {href: '#COLLAPSE_1', 'data-toggle': 'collapse'}, content: [
            {elem: 'icon'},
            {elem: 'title', content: 'Доход'}
        ]},
        {mix: {block: 'collapse'}, attrs: {id: 'COLLAPSE_1'}, content: [
            {elem: 'body', content: [
                {block: 'row', content: [
                    {block: 'col', content: [
                        {block: 'row', content: [
                            {block: 'col', content: [
                                {block: 'form-title', content: 'Кейс'}
                            ]}
                        ]},
                        require('../form/form-case.bemjson.js'),
                    ]},
                    {block: 'col', content: [
                        {block: 'row', content: [
                            {block: 'col', cls: 'col-4', content: [
                                {block: 'form-title', content: 'Документы'}
                            ]},
                            {block: 'col', cls: 'col-8', content: [
                                {block: 'file-input', content: 'Добавить документ'}
                            ]}
                        ]},
                        require('../form/form-document.bemjson'),
                        require('../form/form-document.bemjson'),
                        {block: 'row', content: [
                            {block: 'col', cls: 'offset-4 col-8', content: [
                                {block: 'file-input', content: 'Добавить документ'}
                            ]}
                        ]},
                    ]}
                ]},
            ]}
        ]}
    ]},
    {block: 'card', content: [
        {elem: 'header', cls: 'collapsed', tag: 'a', attrs: {href: '#COLLAPSE_2', 'data-toggle': 'collapse'}, content: [
            {elem: 'icon'},
            {elem: 'title', content: 'Счета'}
        ]},
        {mix: {block: 'collapse'}, attrs: {id: 'COLLAPSE_2'}, content: [
            {elem: 'body', content: [
                'Счета'
            ]}
        ]}
    ]},
    {block: 'card', content: [
        {elem: 'header', cls: 'collapsed', tag: 'a', attrs: {href: '#COLLAPSE_3', 'data-toggle': 'collapse'}, content: [
            {elem: 'icon'},
            {elem: 'title', content: 'Имущество'}
        ]},
        {mix: {block: 'collapse'}, attrs: {id: 'COLLAPSE_3'}, content: [
            {elem: 'body', content: [
                'Имущество'
            ]}
        ]}
    ]},
    {block: 'card', content: [
        {elem: 'header', cls: 'collapsed', tag: 'a', attrs: {href: '#COLLAPSE_4', 'data-toggle': 'collapse'}, content: [
            {elem: 'icon'},
            {elem: 'title', content: 'Авто'}
        ]},
        {mix: {block: 'collapse'}, attrs: {id: 'COLLAPSE_4'}, content: [
            {elem: 'body',  content: [
                'Авто'
            ]}
        ]}
    ]},
];