module.exports = function (bh) {
    bh.match('span', function (ctx, json) {
        ctx
            .tag('span')
            .attr('href', '#')
            .bem(!!Object.keys(ctx.mods()).length);
    })
}